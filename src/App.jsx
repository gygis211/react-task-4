import React, { useReducer } from "react";
// import { Provider } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";

import NavigationMenu from "./components/NavigationMenu.jsx";
import HomePage from "./components/HomePage";
import Admin from './components/AdminPage';
import NewsPage from "./components/NewsPage";
import Profile from "./components/ProfilePage";

import Context from "./utils/context.js";
import * as NEWSACTIONS from "./actionCreator.js/news.action";
import { newsListReducer, defaultNewsState } from "./reducers/news.reducer";

import * as USERACTIONS from "./actionCreator.js/profile.action";
import { profileReducer, defaultUserState } from "./reducers/profile.reducer"


import { homeReducer, defaultHomepageState } from "./reducers/home.reducer"


function App() {

  const [stateNews, dispatchNewsListReducer] = useReducer(newsListReducer, defaultNewsState);
  const addNews = (data) => {
    dispatchNewsListReducer(NEWSACTIONS.addNews(data));
  };
  const findNews = (data) => {
    dispatchNewsListReducer(NEWSACTIONS.findNews(data));
  };
  const editNews = (data) => {
    dispatchNewsListReducer(NEWSACTIONS.editNews(data));
  }
  const deleteNews = (data) => {
    dispatchNewsListReducer(NEWSACTIONS.deleteNews(data));
  }

  const [stateUser, dispatchUsertReducer] = useReducer(profileReducer, defaultUserState)
  const editUser = (data) => {
    dispatchUsertReducer(USERACTIONS.editUser(data));
  }

  const [stateHome] = useReducer(homeReducer, defaultHomepageState)


  return (
    <BrowserRouter>
      <NavigationMenu />
      <Context.Provider 
      value = 
      {{
        homeState: stateHome
      }}
      >
        <Route exact path="/" component={HomePage} />
      </Context.Provider>

      <Context.Provider
        value =
        {{
          newsState: stateNews,
          addNews: (data) => addNews(data),
          findNews: (data) => findNews(data),
          editNews: (data) => editNews(data),
          deleteNews: (data) => deleteNews(data)
        }}
        >
        <Route path="/admin" component={Admin} />
        <Route path="/news" component={NewsPage} />
      </Context.Provider>

      <Context.Provider 
      value = 
      {{
        userState: stateUser,
        editUser: (data) => editUser(data)
      }}
      >
        <Route path="/profile" component={Profile} />
      </Context.Provider>
    </BrowserRouter>
  );
}

export default App;
