import { ADD_NEWS, EDIT_NEWS, DELETE_NEWS, FIND_NEWS } from "../constants/news.constants";

export const addNews = (data) => ({type: ADD_NEWS, data});
export const editNews = (data) => ({type: EDIT_NEWS, data});
export const deleteNews = (data) => ({type: DELETE_NEWS, data});
export const findNews = (data) => ({type: FIND_NEWS, data});