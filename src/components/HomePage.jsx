import React, { useContext } from "react";
// import { connect } from "react-redux";
import Context from "../utils/context.js";

function ProfilePage(props) {
    const context = useContext(Context)

    return (
        <div>
            <div>
                <div className="post" >
                    <h1 className="homePageTitle" >{context.homeState.title}</h1>
                    <img className="homePageImage" src={context.homeState.picture} alt={context.homeState.title}></img>
                    <div className="homePageDescription">{context.homeState.description}</div>
                </div>
            </div>
        </div>
    )
}

// const mapStateToProps = (state) => ({
//     profile: state.profile
// });

// const mapDispatchToProps = (dispatch) => ({
//     editUser: (data) => {dispatch({ type: 'EDIT_USER', data })},
// });

// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(ProfilePage);

export default ProfilePage;

