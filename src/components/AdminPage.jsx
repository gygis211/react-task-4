import React, { useEffect, useState, useContext } from "react";
// import { connect } from "react-redux";
import Context from "../utils/context.js";

function Admin(props) {
  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [picture, setImage] = useState();
  const [isEdit, setIsEdit] = useState(false);
  const context = useContext(Context)

  useEffect(() => { }, [name, description, picture]);

  const sendNewNews = () => {
    let newsList = {
      name: name,
      description: description,
      picture: picture,
    };

    isEdit ?
    context.editNews(newsList) :
    context.addNews(newsList);

    setName("");
    setDescription("");
    setImage("");
    setIsEdit(false);
  };

  const handleEdit = (newsList) => {
    setIsEdit(true);
    setName(newsList.name);
    setDescription(newsList.description);
    setImage(newsList.picture);
  };

  const handleDelete = (newsList) => {
    context.deleteNews(newsList.id);
  };


  return (
    <div>
      <div>
        <h1>Создать товар</h1>
      </div>
      <div className="post">
        <span>Название: </span>{" "}
        <input className="inputForm" value={name} onChange={(e) => setName(e.target.value)} />
        <br />
        <span>Изображение: </span>{" "}
        <input className="inputForm" value={picture} onChange={(e) => setImage(e.target.value)} />
        <br />
        <span>Описание: </span>{" "}
        <textarea className="inputForm" value={description} onChange={(e) => setDescription(e.target.value)} />
        <br />
        <button onClick={sendNewNews}>Отправить</button>
      </div>

      <div>
      {context.newsState.map(newsList => (
          <div className="post" key={newsList.id}>
            <div>
              <h1 className="news">{newsList.name}</h1>
              <img className="img" src={newsList.picture} alt={newsList.name} />
              <p>{newsList.description}</p>
            </div>
            <button onClick={() => handleEdit(newsList)}>Изменить</button>
            <button onClick={() => handleDelete(newsList)}>Удалить</button>
          </div>
        ))}
      </div>
    </div>
  );
}

// const mapStateToProps = (state) => ({
//   news: state.news,
// });

// const mapDispatchToProps = (dispatch) => ({
//   addNews: (data) => {
//     dispatch({ type: "ADD_NEWS", data });
//   },
//   editNews: (data) => {
//     dispatch({ type: "EDIT_NEWS", data });
//   },
//   deleteNews: (id) => {
//     dispatch({ type: "DELETE_NEWS", id });
//   },
// });

// export default connect(mapStateToProps, mapDispatchToProps)(Admin);

export default Admin;
