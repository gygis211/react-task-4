import React, { useState, useContext } from "react";
// import { connect } from "react-redux";
import Context from "../utils/context.js";

function NewsPage(props) {
  const [searchingNews, setSearchingNews] = useState();
  const context = useContext(Context)

  function findNewsByName() {
    context.findNews(searchingNews);
  }

  return (
    <div>
      <h1>Лента новостей</h1>
      <div className="post"> 
      Поиск новости
        <input
          className="inputForm"
          value={searchingNews}
          onChange={(e) => setSearchingNews(e.target.value)}
        />
      <button onClick={findNewsByName}>Найти</button>
      </div>
      {context.newsState.map((object) => (
        <div className="post" key={object.id}>
          <h1 className="news">{object.name}</h1>
          <img className="img" src={object.picture} alt={object.name} />
          <p>{object.description}</p>
        </div>
      ))}
    </div>
  );
}

// const mapStateToProps = (state) => ({
//   news: state.news,
// });

// const mapDispatchToProps = (dispatch) => ({
//   findNews: (data) => {
//     dispatch({ type: "FIND_NEWS", data: data });
//   },
// });

// export default connect(mapStateToProps, mapDispatchToProps)(NewsPage);

export default NewsPage;
