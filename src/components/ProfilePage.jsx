import React, { useState, useContext } from "react";
// import { connect } from "react-redux";
import Context from "../utils/context.js";

function ProfilePage(props) {
    const [editedName, setName] = useState();
    const [editedSurname, setSurname] = useState();
    const [editedCard, setCard] = useState();
    const context = useContext(Context)

    const handleEdit = () => {
        let user = {
            name: editedName ? editedName : context.userState.name,
            surname: editedSurname ? editedSurname : context.userState.surname,
            card: editedCard ? editedCard : context.userState.card,
        }

        context.editUser(user)

        setName('')
        setSurname('')
        setCard('')

    }

    return (

        <div>
            <h1>Данные пользователя</h1>
            <div className="post">
                <div className="inputForm">
                    <strong>Имя: <br />{context.userState.name}</strong><br />
                    <strong>Фамилия: <br />{context.userState.surname}</strong><br />
                    <strong>Номер карты: <br />{context.userState.card}</strong>
                </div>
                Имя <input className="inputForm" value={editedName} onChange={(e) => setName(e.target.value)} />
                фамилия <input className="inputForm" value={editedSurname} onChange={(e) => setSurname(e.target.value)} />
                Номер карты <input className="inputForm" value={editedCard} onChange={(e) => setCard(e.target.value)} />
                <button onClick={handleEdit}>Изменить</button>
            </div>
        </div>
    )
}

// const mapStateToProps = (state) => ({
//     profile: state.profile
// });

// const mapDispatchToProps = (dispatch) => ({
//     editUser: (data) => { dispatch({ type: 'EDIT_USER', data }) },
// });

// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(ProfilePage);

export default ProfilePage;