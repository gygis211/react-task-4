import { EDIT_USER } from "../constants/user.constants";

export const defaultUserState = {
    id: 1,
    name: "Vasya", 
    surname:'Pupkin', 
    card: "1234 5678 9102 3456",
};

export function profileReducer (state = defaultUserState, action) {
    switch (action.type) {
        case EDIT_USER:
            let newState = {
                name: action.data.name,
                surname: action.data.surname,
                card: action.data.card
            };

            return newState;
        default:
            return state;
    }
};
